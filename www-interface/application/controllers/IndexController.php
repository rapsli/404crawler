<?php

class IndexController extends Zend_Controller_Action
{
    protected $_config = array();
    public function init()
    {
        if(!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('auth/login');
        }
        
        /* Initialize action controller here */
        $this->_config = Zend_Registry::get('config');
    }

    public function indexAction()
    {
        
        $log404 = $this->_getLogFileName('log404');
        
        $logs = new Application_Model_Record404($log404);
        $this->view->table = $logs->getRecord();
        $this->view->table = $this->_makeLinks($this->view->table);
        $this->view->count = $logs->getCount();
        $this->view->lastCheck = $logs->getLastCheck();
        
        $analyticsLog = $this->_getLogFileName('analytics');
        $analytics = new Application_Model_Analytics($analyticsLog);
        $this->view->analytics = $analytics->getJsonFormattedLogfile();
        $this->view->section = $this->_getSection();
    }
    
    public function downloadAction() 
    {
        $logfile = $this->_getLogFileName('log404');
        $ar = explode('/', $logfile);
        $filename = $ar[count($ar)-1];
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="'.$filename.'.csv"');
        
        readfile($logfile);
        // disable layout and view
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }

	/**
		A little helper function to get the table ready for the view.
		-> convert data into links
	*/
    private function _makeLinks($table) {
        $helper = $this->view->getHelper('link');
        $first = array_shift($table);
        foreach ($table as &$value) {
                $value[1] = $helper->link($value[1], $value[1]);
                $value[2] = $helper->link($value[2], $value[2]);
        }
        array_unshift($table, $first);
        return $table;
    }
    
    private function _getLogFileName($type) {
        $sites_config = new Zend_Config_Ini($this->_config->params->config);
        $section = $this->_getSection();
        return $sites_config->$section->$type;
    }
    
    private function _getSection()
    {
        $sites_config = new Zend_Config_Ini($this->_config->params->config);
        $section = '';
        if ($this->getRequest()->has('section')) {
            $section = $this->getRequest()->get('section');
        }
        else {
            foreach($sites_config as $key => $item) {
                if ($key != 'general') {
                    $section = $key;
                    break;
                }
            }    
        }
        return $section;
    }
    
}