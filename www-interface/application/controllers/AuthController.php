<?php

class AuthController extends Zend_Controller_Action
{
    protected $_config_params;
    
    public function init()
    {
        /* Initialize action controller here */
		$config_params = Zend_Registry::get('config');
		$this->_config_params = $config_params->params;
    }

    public function loginAction()
    {
        // If we're already logged in, just redirect  
        if(Zend_Auth::getInstance()->hasIdentity())  
        {  
            $this->view->message = array(
                            'msg' => 'You are already logged in',
                            'type' => 'alert-info');
            $this->_redirect('/');  
        } 
        
        $loginForm = $this->getLoginForm();
        
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            if ($loginForm->isValid($request->getPost())) {
                $username = $loginForm->getValue('username');
                $password = $loginForm->getValue('password');
                $realm = 'rapsli.ch';

                $auth   = Zend_Auth::getInstance();
                $adapter = new Zend_Auth_Adapter_Digest($this->_config_params->authFile,
                                        $realm,
                                        $username,
                                        $password);
                                        
                $result = $auth->authenticate($adapter);
                $identity = $auth->getIdentity();
                if (!$result->isValid()) {
                    $this->view->message = array(
                                            'msg' => "Username or password are not valid",
                                            'type' => 'alert-success'
                                            );
                }
                else {
                    $authStorage = $auth->getStorage();  
                    $authStorage->write($identity);
                    $this->view->message = array(
                                            'msg' => "Successfully logged in.",
                                            'type' => 'alert-success'
                                            );
                                            
                    $this->_redirect('/');
                }
            }
        }
        $this->view->loginForm = $loginForm;
 
    }
    
    public function logoutAction() {
        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();
        $this->_redirect('/');
    }
    
     /**
     * Create and return the login form
     *
     * @return object
     */
    protected function getLoginForm()
    {
        $username = new Zend_Form_Element_Text('username');
        $username->setLabel('Username:')
                ->setRequired(true)
                ->setDecorators(array(array('ViewScript', array(
                    'viewScript' => '_form/text.phtml',
                    'placement' => false,
                ))));
        $username->setOptions(array(
                                    'placeholder' => 'username...',
                                    'inputtype' => 'text',
                                    )
                                );

        $password = new Zend_Form_Element_Password('password');
        $password->setLabel('Password:')
                ->setRequired(true)
                ->setDecorators(array(array('ViewScript', array(
                    'viewScript' => '_form/text.phtml',
                    'placement' => false,
                ))));
        $password->setOptions(array(
                                    'placeholder' => 'password...',
                                    'inputtype' => 'password',
                                    )
                                );



        $submit = new Zend_Form_Element_Submit('login');
        $submit->setLabel('Login');

        $loginForm = new Zend_Form();
        $loginForm->setAction($this->_request->getBaseUrl().'/login/index/')
                ->setMethod('post')
                ->addElement($username)
                ->addElement($password)
                ->addElement($submit);

        return $loginForm;
    }
 
}