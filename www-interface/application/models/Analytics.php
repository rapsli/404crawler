<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Application_Model_Analytics {
    
    protected $_logfile;
    
    public function __construct($logfile, array $options = null) {
        $this->_logfile = $logfile;
    }
    
    public function getJsonFormattedLogfile()
    {
        $data = $this->getLogfile();
        return $this->formatJson($data);
    }
    
    
    private function getLogfile()
    {
        $table = array();
        if (!file_exists($this->_logfile)) {
            throw new Exception('File does not exist: ' . $this->_logfile);
        }
        try {
            $handle = fopen($this->_logfile, "r");
        } catch (Exception $e) {
            throw new Exception('Could not read the file ' . $this->_logfile);
        }
        
        ini_set("auto_detect_line_endings", 1);
        $linecount = 0;
        $totalLines = $this->countLinesInFile($this->_logfile);
        
        while (($buffer = fgets($handle, 4096)) !== false) {
            if ($linecount < $totalLines - 30 ) {
                continue;
            }
            $ar_line = explode(';', $buffer);
            $table[] = array(trim($ar_line[0]), trim($ar_line[1]));
            $linecount++;
        }
        return $table;
    }
    
    
    private function formatJson(array $data) {
        $string = '[';
        $string .= '["Date", "404s"],';
        foreach ($data as $item) {
            $string .= '["'.date("m-d", $item[0]).'",'.$item[1].'],';
        }
        $string .= ']';
        return $string;
    }
    
    private function countLinesInFile($file) {
        $linecount = 0;
        $handle = fopen($file, "r");
        while(!feof($handle)){
          $line = fgets($handle);
          $linecount++;
        }

        fclose($handle);
        return $linecount;
    }
}