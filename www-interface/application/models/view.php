<html>
<head>
<title>Bad Links List | www.synthes.com</title>
<link rel="stylesheet" href="styles.css" type="text/css" />
</head>

<body>

<div id="page-wrapper">
    	<div id="header">
        	<img class="logo" src="http://www.synthes.com/sites/NA/Style%20Library/Images/Synthes%20Images/logo_top_nav.png" width="123" height="18"/><br/>
            <h1>Broken Links Report</h1>
        </div>

		<div class="content-item">
		
			<?php

function l($link) {
	return '<a href="' . $link .'">' . $link . '</a>';
}

$filename = '../scripts/log404';

$lines = count(file($filename));

print '<h3>Summary</h3>';
print '<p>Count: ' . $lines . '</p>';

$t = filemtime($filename);
print '<p>Last check:' . date("d. F Y, H:i:s", $t) . '</p>';

$handle = fopen($filename, "r");


print '<h3>Pages with possible issues</h3>';
print '<table><tr><th>Status</th><th>Page</th><th>Link</th></tr>';
$counter = 0;
while (($buffer = fgets($handle, 4096)) !== false) {
	$odd_class = 'odd';
	if ($counter % 2 == 0) {
		$odd_class = 'even';
	}
    $ar_line = explode(';', $buffer);
    print '<tr class="'. $odd_class . '">';
    print '<td>' . $ar_line[0] . '</td>';
    print '<td>' . l($ar_line[1]) . '</td>';
    print '<td>' . l($ar_line[2]) . '</td>';
    print '</tr>';
    $counter++;
}
print '</table>';
?>
		
		</div>
		
		<div id="footer">
			<p>For questions, comments and inputs please contact <a href="mailto:schaer.raphael@synthes.com">Raphael</a></p>
		</div>	
	</div>
    
    
    
	</body>



</body>
</html>
