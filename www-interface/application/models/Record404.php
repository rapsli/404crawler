<?php

class Application_Model_Record404 {

    protected $_record, $_logfile, $_count, $_lastCheck;

    public function __construct($logfile, array $options = null) {
        $this->_logfile = $logfile;
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid property');
        }
        return $this->$method();
    }

    public function getRecord() {
        $this->_record = $this->_readData();
        return $this->_record;
    }

    private function _readData() {
        $table = array(array('Status', 'Path', 'Url'));
        if (!file_exists($this->_logfile)) {
            throw new Exception('File does not exist: ' . $this->_logfile);
        }
        try {
            $handle = fopen($this->_logfile, "r");
        } catch (Exception $e) {
            throw new Exception('Could not read the file ' . $this->_logfile);
        }

        while (($buffer = fgets($handle, 4096)) !== false) {
            $ar_line = explode(';', $buffer);
            $line = array($ar_line[0], $ar_line[1], $ar_line[2]);
            $table[] = $line;
        }
        $this->_record = $table;
        return $table;
    }

    public function getCount() {
        $this->_count = count($this->_record) - 1;
        return $this->_count;
    }

    public function getLastCheck() {
        $this->_lastCheck = filemtime($this->_logfile);
        return date("d. F Y, H:i:s", $this->_lastCheck);
    }

}
