<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected $_config;
    
    protected function _initConfig()
    {
        $config = new Zend_Config($this->getOptions(), true);
        Zend_Registry::set('config', $config);
        $this->_config = $config;
        return $config;
    }
        
    protected function _initCache() {
        if (!is_dir($this->_config->cache->path)) {
            exit("The cache folder does not exist: " . $this->_config->cache->path);
        }
        //Set the cache life time and serialization
        $frontendOptions = array('lifetime' => $this->_config->cache->lifetime, 'automatic_serialization' => true);
        //Set the directory where to put the cache files
        $backendOptions = array('cache_dir' => $this->_config->cache->path);
        //Getting a Zend_Cache_Core object and return the object
        $cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
        Zend_Registry::set('Zend_Cache', $cache);
        return $cache;
    }
    
    protected function _initView()
    {
        // Initialize view
        $view = new Zend_View();
        $view->doctype('XHTML5');
        $view->headTitle('404 Logger');
		//$view->addHelperPath('views/helpers', 'Zend_View_Helper_Table');
		
        //Initialize and/or retrieve a ViewRenderer object on demand via the helper broker
		$viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
		$viewRenderer->initView();
		 
		//add the global helper directory path
		//viewRenderer->view->addHelperPath('/var/www/quickstart/application/GlobalViewHelpers');
 
        // Return it, so that it can be stored by the bootstrap
        return $view;
    }
	
}

