<?php

/**
    this class takes the csv file, parses it and
    produces markup for a Twitter Bootstrap navigation
*/
class Zend_View_Helper_Navigation extends Zend_View_Helper_Abstract {
    
    //protected $_navigation;
    protected $_config;
    protected $_sites_config;
    public function __construct() {
        
    }
    
    public function navigation() {
        $cache = Zend_Registry::get('Zend_Cache');
        
        if(!$navigation = $cache->load('navigation_rendered') && 1==2) {
            $navigation = $this->renderNavigation();
            $cache->save($navigation);
        }
        return $navigation;
    }
    
    
    /**
        Let's render it. We make it so that it matches Twitter
        Bootstrap markup and will look like a nice dropdown
        menu
    */
    protected function renderNavigation() {
        $this->_config = Zend_Registry::get('config');
        $this->_sites_config = new Zend_Config_Ini($this->_config->params->config);
        
        $nav = '';
        $nav .= '<ul class="nav">';
        foreach ($this->_sites_config as $key => $item) {
            if ($key != 'general') {
                $nav .= '<li><a href="?section='.$key.'">'. $item->site . '</a></li>';
            }
        }
        $nav .= '</ul>';
        return $nav;
    }

}