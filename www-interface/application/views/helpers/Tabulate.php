 <?php

class Zend_View_Helper_Tabulate extends Zend_View_Helper_Abstract
{
    public function tabulate ($data, $attribs = array())
    {
		$attribs += array(
			'html' => false,
		);
        $attribString = '';
        foreach ($attribs as $key => $value) {
            $attribString .= ' ' . $key .'="' . $value . '"';
        }

        $header = array_shift($data);
        $html = "<table $attribString>\n<tr>\n";
        foreach ($header as $cell) {
            $escapedCell = $this->view->escape($cell);
            $html .= "<th>$escapedCell</th>\n";
        }

        $html .= "</tr>\n";
        foreach ($data as $row) {
            $html .= "<tr>\n";
            foreach ($row as $cell) {
                $escapedCell = ($attribs['html'] == true? $cell : $this->view->escape($cell));
                $html .= "<td>$escapedCell</td>\n";
            }
            $html .= "</tr>\n";
        }
        $html .= '</table>';
        return $html;
    }
}