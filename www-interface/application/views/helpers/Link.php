 <?php

class Zend_View_Helper_link extends Zend_View_Helper_Abstract
{
    public function link ($link, $text, $attr = array())
    {
        $attr += array(
			'alt' => '',
			'title' => '',
		);
		$link = '<a href="'.$link.'" alt="'.$attr['alt'].'" title="'.$attr['title'].'">' . $text .'</a>';
        return $link;
    }
}