 <?php

class Zend_View_Helper_Msg extends Zend_View_Helper_Abstract
{
    /**
        have an array with the structure:
        array (
            'type' => 'alert-error' //following options: alert-error, alert-success, alert-info
            'msg' => 'there was an error
        )
    */
    public function msg ($msg=array())
    {
        $out = '';
        if (count($msg) > 0) {
            $out .= '<div class="alert '. $msg['type'] .'">';
            $out .= $msg['msg'];
            $out .= '</div>';
        }
        return $out;
    }
}