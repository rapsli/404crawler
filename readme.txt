Readme
------------------------------------------------
404 Crawler has two components:
0. General stuff
1. A python crawler
2. A PHP visualizer based on Zend Framework

Read how to get those two guys up and running

0. General stuff
-------------------------------------------------
The username is: guest
The password is: mylogin

You can change this in application/configs/passwords.txt.
use the following pattern:
username:rapsli.ch:md5(username:rapsli.ch:password)

1. Python Crawler
-------------------------------------------------
The crawler is located in the crawler folder. To run it
you need to first rename and update the config file:

rename crawler/config_demo.ini to crawler/config.ini

Should be easy enough.

Once you have set those values, start the script from the
linux command line: 

python main.py

It's probably best to have it run periodically via cronjob.


2. PHP Visualizer
-------------------------------------------------
save the www-interface folder on your webserver (probably
apache). The structure should then look something like:
/
 - application
 - library
 - public
 - tests
 
Edit the file application/configs/application.ini
set params.config to point to your config.ini file.
 
Download the Zend Framework
Direct link: http://framework.zend.com/releases/ZendFramework-1.11.12/ZendFramework-1.11.12-minimal.zip
or http://framework.zend.com/download/current/

Unpack the ZIP file and goto: /ZendFramework-1.11.12-minimal/library
there should be a folder called "Zend" that contains lots of
folders. Copy the folder "Zend" and past it into 
library. The structure should then look like:

/
 - application
 - library
   - Zend
     - Acl
     - Amf
     - ...
 - public
 - tests
 
Now point your browser to www.yoursite.com/public/ You are good
to go.

===================================================

For questions and comments:
http://twitter.com/rapsli
http://rapsli.ch
https://bitbucket.org/rapsli/404crawler