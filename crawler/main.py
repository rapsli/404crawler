#!/usr/local/bin/python

from parser import SynParser
from HTMLParser2 import HTMLParseError
from logger import Logger
import ConfigParser
import os
import mailer

def main():
    Config = ConfigParser.ConfigParser()
    fn = os.path.join(os.path.dirname(__file__), 'config.ini')
    Config.read(fn)

    for item in Config.sections():
        if item == "general": #only loop through sites config
            continue
        site = ConfigSectionMap(item, Config)['site']
        link = ConfigSectionMap(item, Config)['link']
        analytics = ConfigSectionMap(item, Config)['analytics']
        log404 = ConfigSectionMap(item, Config)['log404']

        logger = Logger(site)
        logger.setLogError(ConfigSectionMap(item, Config)['log_error'])
        logger.setLog404(log404)

        mySpider = SynParser(site, logger)
        counter = 0
        while link != '':
            print 'counter checking file: link ', counter, link
            html = mySpider.getHtmlFile(site, link)
            mySpider.setCurPage(link)
            try:
                mySpider.feed(html)
            except HTMLParseError, e:
                print e
            link = mySpider.getNextPage()
            counter += 1

        if ConfigSectionMap("general", Config)['send_mail'].lower() == "true":
            mailer.send_404_log(ConfigSectionMap(item, Config)['log404'])
        print 'done with - ' + site
        logger.updateAnalytics(analytics, len(open(log404).readlines()))


# a little helper function to access our config file
def ConfigSectionMap(section, Config):
    dict1 = {}
    options = Config.options(section)
    for option in options:
        try:
            dict1[option] = Config.get(section, option)
            if dict1[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None
    return dict1
    
# this is our main function to start everything
if __name__ == '__main__':
    main()
