#!/usr/local/bin/python

from urlparse import urljoin
import time

class Logger():
    log404 = ''
    logError = ''
    site = ''
    
    def __init__(self, site=""):
        self.site = site
    
    def setLogError(self, logError):
        self.logError = logError
        
    def setLog404(self, log404):
        self.log404 = log404
        open(self.log404, 'w+').close()

    def write404Log(self, curPage, link, status=404):
        log = open(self.log404, 'a+')
        log.write(str(status) + "; "+urljoin('http://'+self.site,curPage)+"; "+urljoin('http://'+self.site,link)+"\n")
        log.close()
        
    def writeError(self, error):
        log = open(self.logError, 'a+')
        log.write(error + "\n")
        log.close()
        print error

    def updateAnalytics(self, filename, count):
        with open(filename, "a") as myfile:
            myfile.write(str(int(time.time()))+";"+str(count)+"\n")
        
