﻿#!/usr/local/bin/python
# -*- coding: utf-8 -*-

from HTMLParser2 import HTMLParser
from logger import Logger
import httplib
import urllib
import urllib2
import urlparse
from queue import SynDict
from urlparse import urldefrag


class SynParser(HTMLParser):
    
    logger = ''
    site = ''
    ignores =  [ "#","?", "javascript", "/_layouts/", "mailto:", "login_form", "sendto", "orig_query", "ploneglossary", "search", "search", "language" , "Language" ]
    checkedQueue = SynDict() # Dict to secure uniqness and a bid faster.
    parsedQueue = SynDict()
    openQueue = []
    curPage = ''

    def __init__(self, site, logger=""):
        if logger == "":
            logger = Logger(site)
        HTMLParser.__init__(self)
        self.site = site
        self.logger = logger
    
    def setCurPage(self, curPage):
        self.curPage = curPage
        
    def getNextPage(self):
        if self.openQueue == []:
            return ""
        return self.openQueue.pop(0)
    
    def handle_starttag(self, tag, attrs):
        if tag == 'a' or tag == 'area':
            for at in attrs:
                if at[0] == 'href':
                    newLink = at[1].replace(" ", "%20")
                    for ignore in self.ignores: # ignore some stuff
                        if newLink.rfind(ignore) > -1:
                            return
                    if self.checkedQueue.has_key(newLink):
                        return
                    else: #haven't checked anything yet
                        status = self.isLinkAccessible(self.site, newLink)
                        if status == True:
                            self.checkedQueue[newLink] = 1
                            if not (newLink.rfind(self.site) < 0 and newLink.startswith('http:')):
                                if not newLink.endswith('.pdf'):
                                    self.openQueue.append(newLink)
                        else:
                            self.checkedQueue[newLink] = 2
                            self.logger.write404Log(self.curPage, newLink, status)
    
    """
        Lets check if a link is working or not
        return True if ok
        return the status if not found
    """
    def isLinkAccessible(self, site="", page=""):

        # we are building an absolute URL
        url = page
        if not page.startswith('http'):
            url = "http://"+site+page
        url = self.url_fix(url)

        # http://stackoverflow.com/questions/1140661
        protocol, host, path = urlparse.urlparse(url)[0:3]    # elems [1] and [2]
        try:
            if protocol == 'https':
                conn = httplib.HTTPSConnection(host)
            else:
                conn = httplib.HTTPConnection(host)
            conn.request('HEAD', path)
            status = conn.getresponse().status
            good_codes = [httplib.OK, httplib.FOUND, httplib.MOVED_PERMANENTLY]
            if status in good_codes:
                return True
            else:
                # it might be a http redirect: let's check before we are sure
                # that its not accessible
                if protocol == 'http':
                    return self.isLinkAccessible('','https://'+host+path)
                else:
                    return status
        except StandardError:
            return None

    def getHtmlFile(self, site, page):
        httpconn = httplib.HTTPConnection(site)
        try:
            defraged = urldefrag(page)[0]
            httpconn.request("GET", defraged)
            resp = httpconn.getresponse()
            if resp.msg.type.rfind("text/html") > -1:
                return resp.read()
        except Exception, e:
            self.logger.writeError(str(e))
        return ""
    

    def url_fix(self, s, charset='utf-8'):
        """Sometimes you get an URL by a user that just isn't a real
        URL because it contains unsafe characters like ' ' and so on.  This
        function can fix some of the problems in a similar way browsers
        handle data entered by the user:

        >>> url_fix(u'http://de.wikipedia.org/wiki/Elf (Begriffsklärung)')
        'http://de.wikipedia.org/wiki/Elf%20%28Begriffskl%C3%A4rung%29'

        :param charset: The target charset for the URL if the url was
                        given as unicode string.
        """
        if isinstance(s, unicode):
            s = s.encode(charset, 'ignore')
        scheme, netloc, path, qs, anchor = urlparse.urlsplit(s)
        path = urllib.quote(path, '/%')
        qs = urllib.quote_plus(qs, ':&=')
        return urlparse.urlunsplit((scheme, netloc, path, qs, anchor))