#!/usr/local/bin/python
# -*- coding: utf-8 -*-

import httplib
import time
import urllib
from urlparse import urljoin, urlparse, urldefrag
from urllib import FancyURLopener
from logger import Logger
from parser import SynParser
from HTMLParser2 import HTMLParseError
import httplib

site = 'www.synthes.com'


page = '/sites/NA/Documents/Helpline%20Brochure%20-%20LAT%20-%20Español.pdf'
page = 'http://productlit.synthes.com/KYO_US/kyo_us_trauma/Synthes%20North%20America/Product%20Support%20Materials/Catalogs/SPINE/2011%20Spine%20Cat%20Section%205%20Screw,%20Hook,%20Rod,%20Clamp%20Systems.pdf'
#page = 'https://www.facebook.com'
#page = 'http://productlit.synthes.com/ecommerce/'
#page = 'http://en.wikipedia.org/wiki/DePuy'
page = 'http://www.synthes.com/sites/intl/Products/CMF/Thorax/Pages/Titanium-Sternal-Fixation-Syst.aspx'

logger = Logger(site)
logger.setLogError("/tmp/error")
logger.setLog404("/tmp/404")


mySpider = SynParser(site, logger)

print mySpider.isLinkAccessible(site,  page)

