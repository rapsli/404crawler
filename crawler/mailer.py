#!/usr/bin/python   

import smtplib 
from email.MIMEText import MIMEText   
import ConfigParser
import os

def _send_email(subject, message, from_addr, to_addr, GMAIL_LOGIN, GMAIL_PASSWORD): 
    msg = MIMEText(message) 
    msg['Subject'] = subject 
    msg['From'] = from_addr 
    msg['To'] = to_addr   
    server = smtplib.SMTP('smtp.gmail.com',587) #port 465 or 587 

    server.ehlo() 
    server.starttls() 
    server.ehlo() 
    server.login(GMAIL_LOGIN,GMAIL_PASSWORD) 
    server.sendmail(from_addr, to_addr, msg.as_string()) 
    server.close()     

def send_404_log(log):
    Config = ConfigParser.ConfigParser()
    fn = os.path.join(os.path.dirname(__file__), 'config.ini')
    Config.read(fn)

    sender = ConfigSectionMap("general", Config)['sender']
    receiver = ConfigSectionMap("general", Config)['receiver']
    gmail_user = ConfigSectionMap("general", Config)['gmail_login']
    gmail_password = ConfigSectionMap("general", Config)['gmail_password']

    message = "The following pages contain bad links\n"
    message += "Status | Page | Url\n"
    f = open(log,'r')
    for line in f.readlines():
        message += line
    f.close()
    
    _send_email('Bad link Report', message, sender, receiver, gmail_user, gmail_password)


#Read more: http://halotis.com/2009/07/11/sending-email-from-python-using-gmail/

# a little helper function to access our config file
def ConfigSectionMap(section, Config):
    dict1 = {}
    options = Config.options(section)
    for option in options:
        try:
            dict1[option] = Config.get(section, option)
            if dict1[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None
    return dict1